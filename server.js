/* eslint-disable import/order */
/* eslint-disable no-console */
const http = require('http')
const eetase = require('eetase')
const socketClusterServer = require('socketcluster-server')
const Koa = require('koa')
const koaLogger = require('koa-logger')
const uuid = require('uuid')
const sccBrokerClient = require('scc-broker-client')
const { Nuxt, Builder } = require('nuxt')
require('dotenv').config()

const { PlayerManager } = require('./server/libs/players')

const ENVIRONMENT = process.env.NODE_ENV || 'development'
const ISDEV = ENVIRONMENT === 'development'

const logger = require('pino')({
  prettyPrint: ISDEV
})

// SocketCluster settings

const SOCKETCLUSTER_PORT = process.env.SOCKETCLUSTER_PORT || 8000
const SOCKETCLUSTER_WS_ENGINE = process.env.SOCKETCLUSTER_WS_ENGINE || 'ws'
const SOCKETCLUSTER_SOCKET_CHANNEL_LIMIT =
  Number(process.env.SOCKETCLUSTER_SOCKET_CHANNEL_LIMIT) || 1000
const SOCKETCLUSTER_LOG_LEVEL = process.env.SOCKETCLUSTER_LOG_LEVEL || 2

const SCC_INSTANCE_ID = uuid.v4()
const SCC_STATE_SERVER_HOST = process.env.SCC_STATE_SERVER_HOST || null
const SCC_STATE_SERVER_PORT = process.env.SCC_STATE_SERVER_PORT || null
const SCC_MAPPING_ENGINE = process.env.SCC_MAPPING_ENGINE || null
const SCC_CLIENT_POOL_SIZE = process.env.SCC_CLIENT_POOL_SIZE || null
const SCC_AUTH_KEY = process.env.SCC_AUTH_KEY || null
const SCC_INSTANCE_IP = process.env.SCC_INSTANCE_IP || null
const SCC_INSTANCE_IP_FAMILY = process.env.SCC_INSTANCE_IP_FAMILY || null
const SCC_STATE_SERVER_CONNECT_TIMEOUT =
  Number(process.env.SCC_STATE_SERVER_CONNECT_TIMEOUT) || null
const SCC_STATE_SERVER_ACK_TIMEOUT =
  Number(process.env.SCC_STATE_SERVER_ACK_TIMEOUT) || null
const SCC_STATE_SERVER_RECONNECT_RANDOMNESS =
  Number(process.env.SCC_STATE_SERVER_RECONNECT_RANDOMNESS) || null
const SCC_PUB_SUB_BATCH_DURATION =
  Number(process.env.SCC_PUB_SUB_BATCH_DURATION) || null
const SCC_BROKER_RETRY_DELAY =
  Number(process.env.SCC_BROKER_RETRY_DELAY) || null

const agOptions = {}

if (process.env.SOCKETCLUSTER_OPTIONS) {
  const envOptions = JSON.parse(process.env.SOCKETCLUSTER_OPTIONS)
  Object.assign(agOptions, envOptions)
}

// Nuxt settings
const config = require('./nuxt.config.js')
config.dev = ISDEV
const nuxtInstance = new Nuxt(config)

const koaApp = new Koa()
const httpServer = eetase(http.createServer(koaApp.callback()))
const agServer = socketClusterServer.attach(httpServer, agOptions)

if (ISDEV) {
  // Log every HTTP request. See https://github.com/expressjs/morgan for other
  // available formats.
  koaApp.use(koaLogger())
  const builder = new Builder(nuxtInstance)
  builder.build()
}
koaApp.use(async (ctx) => {
  ctx.status = 200
  ctx.respond = false // Mark request as handled for Koa
  ctx.req.ctx = ctx // This might be useful later on, e.g. in nuxtServerInit or with nuxt-stash
  await nuxtInstance.render(ctx.req, ctx.res)
})

// Add GET /health-check express route
// koaApp.get('/health-check', (req, res) => {
//   ctx.status = 200
//   .send('OK')
// })

// HTTP request handling loop
// ;(async () => {
//   for await (const requestData of httpServer.listener('request')) {
//   }
// })()

// SocketCluster/WebSocket connection handling loop.

const globalPlayersManager = new PlayerManager()

;(async () => {
  for await (const { socket } of agServer.listener('connection')) {
    ;(async () => {
      for await (const req of socket.procedure('login')) {
        let player = globalPlayersManager.get(req.data.uid)
        if (req.id && typeof player !== 'undefined') {
          player.stale(false)
          logger.info(player, 'Player is not stale anymore')
          req.end(player)
        }
        const playerUuid = uuid.v4()
        player = globalPlayersManager.add(playerUuid, req.data)
        logger.info(player, 'A new player connected')
        socket.playerId = player.uid
        req.end(player)
      }
    })()
    ;(async () => {
      for await (const req of socket.listener('close')) {
        const player = globalPlayersManager.get(socket.playerId)
        player.stale(true)
        logger.info(player, "Player can't be joined")
        // req.end({ id: player.id })
      }
    })()
  }
})()

httpServer.listen(SOCKETCLUSTER_PORT)

if (SOCKETCLUSTER_LOG_LEVEL >= 1) {
  ;(async () => {
    for await (const { error } of agServer.listener('error')) {
      logger.error(error)
    }
  })()
}

if (SOCKETCLUSTER_LOG_LEVEL >= 2) {
  logger.info(
    `SocketCluster worker with PID ${process.pid} is listening on port ${SOCKETCLUSTER_PORT}`
  )
  ;(async () => {
    for await (const { warning } of agServer.listener('warning')) {
      logger.warn(warning)
    }
  })()
}

// function colorText(message, color) {
//   if (color) {
//     return `\x1b[${color}m${message}\x1b[0m`
//   }
//   return message
// }

if (SCC_STATE_SERVER_HOST) {
  // Setup broker client to connect to SCC.
  const sccClient = sccBrokerClient.attach(agServer.brokerEngine, {
    instanceId: SCC_INSTANCE_ID,
    instancePort: SOCKETCLUSTER_PORT,
    instanceIp: SCC_INSTANCE_IP,
    instanceIpFamily: SCC_INSTANCE_IP_FAMILY,
    pubSubBatchDuration: SCC_PUB_SUB_BATCH_DURATION,
    stateServerHost: SCC_STATE_SERVER_HOST,
    stateServerPort: SCC_STATE_SERVER_PORT,
    mappingEngine: SCC_MAPPING_ENGINE,
    clientPoolSize: SCC_CLIENT_POOL_SIZE,
    authKey: SCC_AUTH_KEY,
    stateServerConnectTimeout: SCC_STATE_SERVER_CONNECT_TIMEOUT,
    stateServerAckTimeout: SCC_STATE_SERVER_ACK_TIMEOUT,
    stateServerReconnectRandomness: SCC_STATE_SERVER_RECONNECT_RANDOMNESS,
    brokerRetryDelay: SCC_BROKER_RETRY_DELAY
  })

  if (SOCKETCLUSTER_LOG_LEVEL >= 1) {
    ;(async () => {
      for await (const { error } of sccClient.listener('error')) {
        error.name = 'SCCError'
        logger.error(error)
      }
    })()
  }
}
