const events = require('events')
class Player {
  constructor(uid, meta) {
    this.uid = uid
    this.meta = meta
    return this
  }

  stale(bool) {
    this.staling = bool
  }

  set(property, data) {
    this.meta[property] = data
    return this
  }
}

class PlayerManager {
  constructor() {
    this.players = new Map()
    this.events = new events.EventEmitter()
  }
  add(uid, meta) {
    const newPlayer = new Player(uid, meta)
    this.players.set(uid, newPlayer)
    this.events.emit('playerAdded', newPlayer)
    return newPlayer
  }

  get(uid) {
    return this.players.get(uid)
  }

  findWithPredicate(predicate) {
    let foundElm = null
    this.players.forEach((elm) => {
      if (predicate(elm) === true) {
        foundElm = elm
      }
    })
    return foundElm
  }

  getByName(name) {
    return this.findWithPredicate((elm) => elm.data.name === name)
  }

  setData(id, property, data) {
    const player = this.get(id)
    player.set(property, data)
    this.events.emit('playerModified', player)
    return player
  }
}

module.exports = {
  Player,
  PlayerManager
}
