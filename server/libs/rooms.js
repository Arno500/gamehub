const events = require('events')
const uuid = require('uuid')
const { PlayerManager } = require('./players')

module.exports = {
  Room: class Room {
    constructor(owner) {
      this.owner = owner
      this.players = new PlayerManager()
      this.id = uuid.v4()
      this.game = null
      this.chat = []
      this.events = new events.EventEmitter()
      this.events.on('messageReceived', this.chat.push)
    }

    setGame(game) {
      this.game = game
    }

    handleChat(data) {
      if (typeof this.game.chatHandler === 'function') {
        // If the chatHandler of the current game returns false, we don't send the messageReceived event
        if (this.game.chatHandler(data) === false) {
          return
        }
      }
      this.events.emit('messageReceived', data)
    }
  }
}
